const socket = io()
const clientsTotal = document.getElementById('client-total')
const messageContainer = document.getElementById('message-container')
const nameInput = document.getElementById('name-input')
const messageForm = document.getElementById('message-form')
const messageInput = document.getElementById('message-input')
const messageButton = document.getElementById('send-button')
const welcomeMessage = document.getElementById('welcome')

let name = prompt('What is Your Name?')
nameInput.value = name;

messageForm.addEventListener('submit', (e) => {
      e.preventDefault()
      sendMessage()
})

socket.on('client-total', (data) => {
      clientsTotal.innerText = `Users: ${data}`
})

function sendMessage() {
      const data = {
            name: nameInput.value,
            message: messageInput.value,
            dateTime: moment().format('MMMM Do YYYY,h:mm:ss a')
      }
      if (messageInput.value==='') return 
      socket.emit('message', data)



      addMessagetoUI(true,data)
      messageInput.value = ""
}

socket.on('chat-message', (data) => {
      addMessagetoUI(false,data)
})

function addMessagetoUI(isOwnMessage,data){
      const element = `
      <ul class="${isOwnMessage ? "message-right" : "message-left"}">

      <p class="message">
      ${data.message}
      <span>${data.name} * ${moment().fromNow()}</span>
      </p>
      </li>`

      messageContainer.innerHTML+=element
      scrollToBottom()
}

function scrollToBottom() {
      messageContainer.scrollTo(0,messageContainer.scrollHeight)
}




