const express = require('express');
require('dotenv').config()
const path = require('path')
const moment = require('moment')
const app = express();
const PORT = process.env.PORT || 3000

app.use(express.static(path.join(__dirname, 'public')))

const server = app.listen(PORT, ()=> {
      console.log('Hello, I\'m running on', PORT);
})
const io = require('socket.io')(server);

let socketsConnected = new Set()
io.on('connection',onConnected)



function onConnected(socket){
      //console.log(socket.id)
      socketsConnected.add(socket.id)

      io.emit('client-total',socketsConnected.size)

      socket.on('disconnect',()=> {
            console.log('Socket Disconnected: ', socket.id)
            socketsConnected.delete(socket.id)
            io.emit('client-total',socketsConnected.size)
      })

      socket.on('message', (data)=> {
            console.log(data)
            socket.broadcast.emit('chat-message', data)
      })
}





